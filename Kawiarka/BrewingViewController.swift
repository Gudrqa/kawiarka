//
//  BrewingViewController.swift
//  Kawiarka
//
//  Created by Alicja on 23/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import UIKit
import CoreData

protocol BrewingViewControllerDelegate{
    func myVCDidFinish(controller: BrewingViewController, text: String)
}

class BrewingViewController: UIViewController {

    private let smakSource = ["Slodki", "Kwaśny", "Gorzki"]
    private let aromatSource = ["Orzechy", "Czekolada", "Cytrusy"]
    
    @IBOutlet weak var smakPickerView: UIPickerView!
    @IBOutlet weak var aromatPickerView: UIPickerView!
    
    
    var delegate:BrewingViewControllerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        smakPickerView.dataSource = self
        aromatPickerView.dataSource = self
        smakPickerView.delegate = self
        aromatPickerView.delegate = self
        
        
        

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "detailsToEditSegue"){
        }
    }
    


}

extension BrewingViewController
: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == smakPickerView{
            return smakSource.count
        }
        else if pickerView == aromatPickerView{
            return aromatSource.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //po wybraniu co sie dzieje np na label idzie
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == smakPickerView{
            return smakSource[row]
        }
        else if pickerView == aromatPickerView{
            return aromatSource[row]
        }
        return "0"
    }
    
}
