//
//  SecondViewController.swift
//  Kawiarka
//
//  Created by Alicja on 19/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import UIKit
import CoreData

class SecondViewController: UIViewController,UITableViewDataSource,UISearchResultsUpdating,BrewingViewControllerDelegate {
    
    func myVCDidFinish(controller: BrewingViewController, text: String) {
        controller.navigationController?.popViewController(animated: true)
    }
    
    //variables
    var ziarnaTab: [NSManagedObject] = []
    var currentTab: [NSManagedObject] = []
    var addBeansViewController: AddBeansViewController?
    var brewingViewController: BrewingViewController?
    @IBOutlet weak var BeansTable: UITableView!
    var searchController: UISearchController!
    
    
    //tablica
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return currentTab.count
        }
        return ziarnaTab.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeansCell", for: indexPath) as! BeansTableViewCell
        let bean: NSManagedObject
        
        if isFiltering(){
            bean = currentTab[indexPath.row]
        }else{
            bean = ziarnaTab[indexPath.row]
        }
        
        cell.name = bean.value(forKeyPath: "nazwa") as? String
        let image = bean.value(forKey: "obrazek") as? NSData
        cell.photo = UIImage(data: image! as Data)
        cell.layoutSubviews()
        return cell
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BeansTable.reloadData()
        currentTab = ziarnaTab
        //BeansTable.delegate = self 
        BeansTable.dataSource = self
        
        
        BeansTable.tableFooterView = UIView(frame: CGRect.zero)
        self.BeansTable.register(BeansTableViewCell.self, forCellReuseIdentifier: "BeansCell")
        self.BeansTable.rowHeight = UITableView.automaticDimension
        self.BeansTable.estimatedRowHeight = 200
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action:Selector(("longPress")))
        self.view.addGestureRecognizer(longPressRecognizer)
        
        
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.barStyle = UIBarStyle.black
        searchController.searchBar.tintColor = UIColor(red: 158/255, green: 119/255, blue: 85/255, alpha: 1.0)
        searchController.searchBar.barTintColor = UIColor(red: 158/255, green: 119/255, blue: 85/255, alpha: 1.0)
        searchController.searchBar.backgroundColor = UIColor(red: 158/255, green: 119/255, blue: 85/255, alpha: 1.0)
        BeansTable.backgroundView = UIView()
        
        BeansTable.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Ziarna")
        
        do{
            ziarnaTab = try managedContext.fetch(fetchRequest)
        }catch let error as NSError{
            print("Could not fetch data. \(error) \(error.userInfo)")
        }
    }
    
    
    
    @IBAction func AddBeansButton(_ sender: Any) {
        performSegue(withIdentifier: "addBeansSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addBeansSegue"{
            if let destVC = segue.destination as? AddBeansViewController{
                addBeansViewController = destVC
            }
        }
        else if segue.identifier == "temp"{
            if let destVC = segue.destination as? BrewingViewController{
                brewingViewController = destVC
            }
        }
    }
    
    func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer){
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began{
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if let indexPath = BeansTable.indexPathForRow(at: touchPoint){
                print("success2")
            }
        }
    }
    
    @IBAction func tempToNext(_ sender: Any) {
        
        performSegue(withIdentifier: "temp", sender: nil)
    }
    
    //funkcje odpowiadajace za wyszukiwanie
    //wymagana
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    func searchBarIsEmpty() -> Bool{
        return searchController.searchBar.text?.isEmpty ?? true
    }
    func filterContentForSearchText(_ searchText: String, scope: String = "All"){
        
        if(searchText.count>3){
            let newStr = searchText[0...2]
            
            let endIdx = searchText.count-1
            let newSearchText = searchText[3...endIdx]
            
            if (newStr == "#k "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "kraj") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#f "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "farma") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#r "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "region") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#w "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: Int = bean.value(forKeyPath: "wysokosc") as? Int ?? 0
                    return tempName == Int(newSearchText)
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#t "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "odmiana") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#o "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "obrobka") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else if(newStr == "#p "){
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "palarnia") as? String ?? "place_holder_kraj"
                    return tempName.lowercased().contains(newSearchText.lowercased())
                })
                BeansTable.reloadData()
            }
            else{
                
                currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                    let tempName: String = bean.value(forKeyPath: "nazwa") as? String ?? "place_holder_nazwa"
                    return tempName.lowercased().contains(searchText.lowercased())
                })
                BeansTable.reloadData()
                
            }
            
            
        }
        else{
            
            currentTab = ziarnaTab.filter({(bean : NSManagedObject) -> Bool in
                let tempName: String = bean.value(forKeyPath: "nazwa") as? String ?? "place_holder_nazwa"
                return tempName.lowercased().contains(searchText.lowercased())
            })
            BeansTable.reloadData()
        }
        
    }
    func isFiltering()->Bool{
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    
    
    

}

extension String{
    
    subscript(bounds: CountableClosedRange<Int>)-> String{
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
}
