//
//  ViewController.swift
//  Kawiarka
//
//  Created by Alicja on 18/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var secondViewController: SecondViewController?
    
    @IBAction func AddBeansButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // navigationController?.navigationBar.barTintColor = UIColor(red: 0x9E, green: 0x77, blue: 0x55, alpha: 1)
        //navigationController?.navigationBar.tintColor = UIColor(displayP3Red: 221/225, green: 218/225, blue: 208/225, alpha: 1)
    }
    @IBAction func Click(_ sender: Any) {
        
        performSegue(withIdentifier: "testSegue", sender: nil)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "testSegue"{
            if let destVC = segue.destination as? SecondViewController{
                secondViewController = destVC
            }
        }
    }
    
    
    @IBAction func FrenchPress(_ sender: Any) {
        
    }
    
    @IBAction func Drip(_ sender: Any) {
    }
    @IBAction func Aeropress(_ sender: Any) {
    }
    
    @IBAction func Ekspres(_ sender: Any) {
    }
    
    @IBAction func Chemex(_ sender: Any) {
    }
    
    @IBAction func Kawiarka(_ sender: Any) {
    }
    
    @IBAction func Cupping(_ sender: Any) {
    }
    
    @IBAction func Inne(_ sender: Any) {
    }
    
}

