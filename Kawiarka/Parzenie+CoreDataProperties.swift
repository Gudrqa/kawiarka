//
//  Parzenie+CoreDataProperties.swift
//  Kawiarka
//
//  Created by Alicja on 23/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//
//

import Foundation
import CoreData


extension Parzenie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Parzenie> {
        return NSFetchRequest<Parzenie>(entityName: "Parzenie")
    }

    @NSManaged public var aromat: String?
    @NSManaged public var czas: Double
    @NSManaged public var doza: Int16
    @NSManaged public var gruboscMielenia: Int16
    @NSManaged public var metoda: String?
    @NSManaged public var preinfuzja: Int16
    @NSManaged public var smak: String?
    @NSManaged public var temperatura: Double
    @NSManaged public var parzenieZiarna: Ziarna?

}
