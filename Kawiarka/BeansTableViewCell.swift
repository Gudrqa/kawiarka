//
//  BeansTableViewCell.swift
//  Kawiarka
//
//  Created by Alicja on 22/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import UIKit

class BeansTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var name : String?
    var photo : UIImage?
    
    var messageView : UITextView = {
        
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        textView.backgroundColor = UIColor(red: 158/255, green: 119/255, blue: 85/255, alpha: 1)
        textView.tintColor = UIColor(red: 221/255, green: 217/255, blue: 208/255, alpha: 1)
        textView.font = UIFont(name: "Times New Roman", size: 25)
        textView.textColor = UIColor(red: 221/255, green: 217/255, blue: 208/255, alpha: 1)
        textView.isEditable = false
        return textView
    }()
    
    var mainImageView : UIImageView = {
        
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    override init(style:UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(mainImageView)
        self.addSubview(messageView)
        
        //self.backgroundColor = UIColor(red: 158/255, green: 119/255, blue: 85/255, alpha: 1)
        //self.tintColor = UIColor(red: 221/255, green: 217/255, blue: 208/255, alpha: 1)
        
        mainImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        mainImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        mainImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        mainImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        mainImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        
        messageView.leftAnchor.constraint(equalTo: self.mainImageView.rightAnchor).isActive = true
        messageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        messageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        messageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let name = name{
            messageView.text = name
        }
        if let image = photo{
            mainImageView.image = image
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
