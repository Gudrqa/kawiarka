//
//  AddBeansViewController.swift
//  Kawiarka
//
//  Created by Alicja on 20/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//

import UIKit
import CoreData

class AddBeansViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(displayP3Red: 158/255, green: 119/225, blue: 85/225, alpha: 1)

        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var beansImageView: UIImageView!
    
    @IBAction func addPhotoBeans(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        
        self.present(image, animated: true){
            //after complete
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            beansImageView.image = image
        }
        else{
            //Error message
        }
        
        self.dismiss(animated:true, completion: nil)
        
    }
    
    @IBOutlet weak var NazwaField: UITextField!
    @IBOutlet weak var KrajField: UITextField!
    @IBOutlet weak var FarmaField: UITextField!
    @IBOutlet weak var RegionField: UITextField!
    @IBOutlet weak var WysokoscField: UITextField!
    @IBOutlet weak var OdmianaField: UITextField!
    @IBOutlet weak var ObróbkaField: UITextField!
    @IBOutlet weak var PalarniaField: UITextField!
    
    @IBAction func AddBeansButton(_ sender: Any) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let beansEntity = NSEntityDescription.entity(forEntityName: "Ziarna", in: managedContext)!
        
        let ziarno = NSManagedObject(entity: beansEntity, insertInto: managedContext)
        
        ziarno.setValue(NazwaField.text, forKeyPath:"nazwa")
        ziarno.setValue(KrajField.text, forKeyPath:"kraj")
        ziarno.setValue(FarmaField.text, forKeyPath:"farma")
        ziarno.setValue(RegionField.text, forKeyPath:"region")
        ziarno.setValue(Int(WysokoscField.text!), forKeyPath:"wysokosc")
        ziarno.setValue(OdmianaField.text, forKeyPath:"odmiana")
        ziarno.setValue(ObróbkaField.text, forKeyPath:"obrobka")
        ziarno.setValue(PalarniaField.text, forKeyPath:"palarnia")
        
        let newImageData = beansImageView.image!
        
        ziarno.setValue(newImageData.pngData(), forKeyPath:"obrazek")
        
        do{
            try managedContext.save()
        } catch let error as NSError{
            print("yh \(error)")
        }
        
        print("success")
        
    }
    
    
}
