//
//  Ziarna+CoreDataProperties.swift
//  Kawiarka
//
//  Created by Alicja on 23/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//
//

import Foundation
import CoreData


extension Ziarna {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ziarna> {
        return NSFetchRequest<Ziarna>(entityName: "Ziarna")
    }

    @NSManaged public var farma: String?
    @NSManaged public var kraj: String?
    @NSManaged public var nazwa: String?
    @NSManaged public var obrazek: NSData?
    @NSManaged public var obrobka: String?
    @NSManaged public var odmiana: String?
    @NSManaged public var palarnia: String?
    @NSManaged public var region: String?
    @NSManaged public var wysokosc: Int64
    @NSManaged public var ziarnaMetody: NSSet?
    @NSManaged public var ziarnaParzenie: NSSet?

}

// MARK: Generated accessors for ziarnaMetody
extension Ziarna {

    @objc(addZiarnaMetodyObject:)
    @NSManaged public func addToZiarnaMetody(_ value: Metody)

    @objc(removeZiarnaMetodyObject:)
    @NSManaged public func removeFromZiarnaMetody(_ value: Metody)

    @objc(addZiarnaMetody:)
    @NSManaged public func addToZiarnaMetody(_ values: NSSet)

    @objc(removeZiarnaMetody:)
    @NSManaged public func removeFromZiarnaMetody(_ values: NSSet)

}

// MARK: Generated accessors for ziarnaParzenie
extension Ziarna {

    @objc(addZiarnaParzenieObject:)
    @NSManaged public func addToZiarnaParzenie(_ value: Parzenie)

    @objc(removeZiarnaParzenieObject:)
    @NSManaged public func removeFromZiarnaParzenie(_ value: Parzenie)

    @objc(addZiarnaParzenie:)
    @NSManaged public func addToZiarnaParzenie(_ values: NSSet)

    @objc(removeZiarnaParzenie:)
    @NSManaged public func removeFromZiarnaParzenie(_ values: NSSet)

}
