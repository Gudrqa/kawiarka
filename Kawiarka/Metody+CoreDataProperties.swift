//
//  Metody+CoreDataProperties.swift
//  Kawiarka
//
//  Created by Alicja on 23/03/2019.
//  Copyright © 2019 Alicja. All rights reserved.
//
//

import Foundation
import CoreData


extension Metody {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Metody> {
        return NSFetchRequest<Metody>(entityName: "Metody")
    }

    @NSManaged public var nazwa: String?
    @NSManaged public var metodyZiarna: NSSet?

}

// MARK: Generated accessors for metodyZiarna
extension Metody {

    @objc(addMetodyZiarnaObject:)
    @NSManaged public func addToMetodyZiarna(_ value: Ziarna)

    @objc(removeMetodyZiarnaObject:)
    @NSManaged public func removeFromMetodyZiarna(_ value: Ziarna)

    @objc(addMetodyZiarna:)
    @NSManaged public func addToMetodyZiarna(_ values: NSSet)

    @objc(removeMetodyZiarna:)
    @NSManaged public func removeFromMetodyZiarna(_ values: NSSet)

}
